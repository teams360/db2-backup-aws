#!/bin/bash
PATH=/bin:/usr/bin:/usr/sbin:/usr/local/bin/
source $HOME/.bash_profile
source $HOME/sqllib/db2profile
set -x

YESTERDAY=`date  --date=yesterday +"%Y-%m-%d"`
TODAY=`date +"%Y-%m-%d"`
DATABASE=DBHARM

BUCKET=s3://prologic-harm-logs
ARCHIVELOGDIR=/db/dbharm/log/archive/tmsinst/$DATABASE
SYNCDIR=/dr/DBHARM/archive/tmsinst/$DATABASE

if [ -f /var/tmp/$DATABASE-logfileship ]; then
	exit 0
	else
	echo "sync will start now"
fi

        createlockfile() {
                touch /var/tmp/$DATABASE-logfileship
        	}

        snaplogs() {
                db2 archive log for db DBHARM
                sleep 30
        	}

        ziplogs() {
		find $SYNCDIR -name *.LOG -exec gzip -f {} \;
                }

	sendlogs() {
		aws s3 sync $SYNCDIR $BUCKET/$TODAY --exclude "*.LOG"
		}

	cleanlogs() {
		find $SYNCDIR -name *.LOG* -mmin +240 -exec rm -f {} \;
		find $ARCHIVELOGDIR -name *.LOG* -mmin +20160 -exec rm -f {} \;
		}

        deletelockfile() {
                rm /var/tmp/$DATABASE-logfileship
        	}



createlockfile
snaplogs
ziplogs
sendlogs
cleanlogs
deletelockfile

