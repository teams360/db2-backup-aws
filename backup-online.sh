#!/bin/bash
PATH=/usr/bin:/bin:/usr/sbin
source $HOME/.bash_profile
source $HOME/sqllib/db2profile
set -x

BACKUPDIR="/backup/`date "+%Y-%m-%d"`"
DATABASE="DBHARM"


	createlockfile() {
                touch /var/tmp/$DATABASE-backup
        }


        prunefiles() {
                cd /backup && find /backup/ -name '201*' -mtime +21 -exec rm -rf {} \;
        }

        backup() {
                for i in $DATABASE
                 do
		   mkdir $BACKUPDIR
		   db2 archive log for db $i
               	   sleep 30
                   db2 connect to $i && db2 backup database $i online to $BACKUPDIR compress include logs without prompting
		   LATESTBACKUP=`cd $BACKUPDIR && ls -tF1 $i* | head -1`
		   chmod a+r $BACKUPDIR/$LATESTBACKUP
                done
        }

        deletelockfile() {
                rm /var/tmp/$DATABASE-backup
        }



createlockfile
prunefiles
backup
deletelockfile
/home/instharm/bin/dr-database.sh
