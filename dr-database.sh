#!/bin/bash
PATH=/bin:/usr/bin:/usr/local/bin
set -x
source $HOME/.bash_profile

BUCKET=s3://prologic-harm-db
BACKUPDIR=/backup
DATABASE=DBHARM
BACKUPFILE=`cd $BACKUPDIR && ls -tF1 | head -1`
DATESTAMP=`cd $BACKUPDIR && ls -tF1 | head -1 | awk -F "." '{ print $6 }'`

if [ -f /var/tmp/$DATABASE-db-sync ]; then
        exit 0
        else
        echo "sync will start now"
fi

        createlockfile() {
                touch /var/tmp/$DATABASE-db-sync
                }

	sendbackup() {
		aws s3 sync $BACKUPDIR $BUCKET
		}

        deletelockfile() {
                rm /var/tmp/$DATABASE-db-sync
                }

createlockfile
sendbackup
deletelockfile

